package com.cqwo.imagestream.helper;

import java.util.Calendar;

public class DateHelper {


    public static Integer getTimestamp() {

        Calendar calendar = Calendar.getInstance();

        return Math.toIntExact(calendar.getTimeInMillis() / 1000);

    }

}
