package com.cqwo.imagestream.helper;


import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.InputStream;
import java.text.MessageFormat;

@Service(value = "UploadUtils")
public class UploadUtils {

    private Auth auth;

    private String upToken;

    private final String accessKey = "EJVQa8CyiPUElQCyotFlqxcmos52OTMzmqAjc0So";
    private final String secretKey = "RM0oGopOUz4gISSeXCfSF8IUFCZTSHJnuPI4EyJs";
    private final String bucket = "imgcdn";
    private final String url = "imgcdn.510link.com";


    private Logger logger = LoggerFactory.getLogger(UploadUtils.class);

    @PostConstruct
    public void init() {
        auth = Auth.create(accessKey, secretKey);
        upToken = auth.uploadToken(bucket);
    }

    /**
     * 文件上传
     *
     * @param file 文件
     * @param key  key
     * @return 返回文件地址
     * @throws QiniuException 文件上传异常
     */
    public String upload(File file, String key) throws QiniuException {

        Response response = getUploadManager().put(file, key, upToken);

        if (response.isOK()) {

            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class); //System.out.println(putRet.key); //System.out.println(putRet.hash);

            return MessageFormat.format("http://{0}/{1}", url, putRet.key);
        }

        throw new QiniuException(response);
    }

    /**
     * 文件流上传
     *
     * @param stream 输入流
     * @param key    key
     * @return 返回文件地址
     * @throws QiniuException 文件上传异常
     */
    public String upload(InputStream stream, String key) throws QiniuException {

        Response response = getUploadManager().put(stream, key, upToken, null, "image/jpeg");

        if (response.isOK()) {

            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class); //System.out.println(putRet.key); //System.out.println(putRet.hash);

            return MessageFormat.format("http://{0}/{1}", url, putRet.key);
        }

        throw new QiniuException(response);
    }

    public UploadManager getUploadManager() {

        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Zone.zone0());
        //...其他参数参考类注释
        return new UploadManager(cfg);
    }


    public String getUpToken() {

        auth = Auth.create(accessKey, secretKey);
        upToken = auth.uploadToken(bucket);

        return upToken;

    }

}
