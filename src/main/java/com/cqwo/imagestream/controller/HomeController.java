package com.cqwo.imagestream.controller;

import java.io.*;
import java.lang.invoke.MethodType;

import com.cqwo.imagestream.helper.DateHelper;
import com.cqwo.imagestream.helper.UploadUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class HomeController {


    @Autowired
    UploadUtils uploadUtils;

    @RequestMapping(value = {"/", "", "index"})
    public String index() {


        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet("http://pics.sc.chinaz.com/files/pic/pic9/201910/zzpic20759.jpg");

        // 响应模型
        CloseableHttpResponse response = null;

        OutputStream outputStream = null;


        try {
            response = httpClient.execute(httpGet);

            System.out.println("响应状态为:" + response.getStatusLine());

            int statusCode = response.getStatusLine().getStatusCode();


            if (statusCode == HttpStatus.SC_OK) {

//                File file = File.createTempFile(DateHelper.getTimestamp().toString(), ".jpg");
//
//                outputStream = new FileOutputStream(file);

                InputStream inputStream = response.getEntity().getContent();

                return uploadUtils.upload(inputStream, "temp/" + DateHelper.getTimestamp().toString() + ".jpg");
//
//                byte buffer[] = new byte[32 * 1024];
//
//                int j;
//
//                while ((j = inputStream.read(buffer)) != -1) {
//                    outputStream.write(buffer, 0, j);
//                }
//
//                outputStream.write(buffer);


                // return file.getAbsolutePath();


            }
            return "视频读取失败";
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {


                if (outputStream != null) {
                    outputStream.close();
                }
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "视频读取失败";
    }

}
